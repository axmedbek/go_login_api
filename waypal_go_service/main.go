package main

import (
	"net/http"
	"waypal_go_service/routes"
)

// @title Waypal Documentation API
// @version 1.0
func main() {
	_ = http.ListenAndServe(":8080", routes.Router())
}
