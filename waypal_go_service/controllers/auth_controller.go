package controllers

import (
	"context"
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"github.com/pressly/chi"
	"go.mongodb.org/mongo-driver/bson"
	"golang.org/x/crypto/bcrypt"
	"log"
	"net/http"
	"strconv"
	"waypal_go_service/config"
	"waypal_go_service/dto"
	"waypal_go_service/models"
)

/**
 * @api {post} /auth/login login
 * @apiName login
 * @apiGroup authentication
 *
 * @apiParam {String} username User unique username.
 * @apiParam {String} password User password.
 *
 * @apiSuccess {String} username User unique username.
 * @apiSuccess {Number} type User type [driver:1 or passenger:2].
 * @apiSuccess {String} token jwt token of the User.
 *
 */
func LoginHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		var result models.User
		var successResponse dto.UserSuccess
		var errorResponse dto.UserError
		var userDTO dto.UserDTO
		var username = r.FormValue("username")
		var password = r.FormValue("password")

		if username == "" || password == "" {
			errorResponse.Status = false
			errorResponse.Message = "Username and password required"
			_ = json.NewEncoder(w).Encode(errorResponse)
			return
		}

		collection, err := config.GetDBCollection()

		if err != nil {
			log.Fatal(err)
		}

		err = collection.FindOne(context.TODO(), bson.D{{"username", username}}).Decode(&result)

		if err != nil {
			errorResponse.Status = false
			errorResponse.Message = "Invalid username"
			_ = json.NewEncoder(w).Encode(errorResponse)
			return
		}

		err = bcrypt.CompareHashAndPassword([]byte(result.Password), []byte(password))

		if err != nil {
			errorResponse.Status = false
			errorResponse.Message = "Invalid password"
			_ = json.NewEncoder(w).Encode(errorResponse)
			return
		}

		token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"username": result.Username,
			"type":     result.Type,
		})

		tokenString, err := token.SignedString([]byte("secret"))

		if err != nil {
			errorResponse.Status = false
			errorResponse.Message = "Error while generating token,Try again"
			_ = json.NewEncoder(w).Encode(errorResponse)
			return
		}

		userDTO.Token = tokenString
		userDTO.Username = result.Username
		userDTO.Type = result.Type

		successResponse.Status = true
		successResponse.Data = userDTO

		_ = json.NewEncoder(w).Encode(successResponse)
	}
}

/**
 * @api {post} /auth/register register
 * @apiName register
 * @apiGroup authentication
 *
 * @apiParam {String} username User unique username.
 * @apiParam {Number} type User type [driver:1 or passenger:2].
 * @apiParam {String} password User password.
 * @apiParam {String} confirm_password User confirm password.

 * @apiSuccess {String} username User unique username.
 * @apiSuccess {Number} type User type [driver:1 or passenger:2].
 * @apiSuccess {String} token jwt token of the User.
 *
 */
func RegisterHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		var successResponse dto.UserSuccess
		var errorResponse dto.UserError
		var user models.User
		user.Username = r.FormValue("username")
		user.Type, _ = strconv.ParseInt(r.FormValue("type"), 10, 64)
		user.Password = r.FormValue("password")

		if user.Username == "" {
			errorResponse.Status = false
			errorResponse.Message = "Username is required"
			_ = json.NewEncoder(w).Encode(errorResponse)
			return
		}

		if user.Password == "" {
			errorResponse.Status = false
			errorResponse.Message = "Password is required"
			_ = json.NewEncoder(w).Encode(errorResponse)
			return
		}

		if user.Type != 1 && user.Type != 2 {
			errorResponse.Status = false
			errorResponse.Message = "Type must be 1 or 2"
			_ = json.NewEncoder(w).Encode(errorResponse)
			return
		}

		collection, err := config.GetDBCollection()

		if err != nil {
			errorResponse.Status = false
			errorResponse.Message = err.Error()
			_ = json.NewEncoder(w).Encode(errorResponse)
			return
		}

		var result models.User
		err = collection.FindOne(context.TODO(), bson.D{{"username", user.Username}}).Decode(&result)

		if err != nil {
			if err.Error() == "mongo: no documents in result" {
				hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), 5)

				if err != nil {
					errorResponse.Status = false
					errorResponse.Message = "Error While Hashing Password, Try Again"
					_ = json.NewEncoder(w).Encode(errorResponse)
					return
				}
				user.Password = string(hash)

				_, err = collection.InsertOne(context.TODO(), user)
				if err != nil {
					errorResponse.Status = false
					errorResponse.Message = "Error While Creating User, Try Again"
					_ = json.NewEncoder(w).Encode(errorResponse)
					return
				}

				token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
					"username": result.Username,
					"type":     result.Type,
				})

				tokenString, err := token.SignedString([]byte("secret"))

				if err != nil {
					errorResponse.Status = false
					errorResponse.Message = "Error while generating token,Try again"
					_ = json.NewEncoder(w).Encode(errorResponse)
					return
				}

				successResponse.Status = true
				successResponse.Data = dto.UserDTO{Username: user.Username, Type: user.Type, Token: tokenString}
				_ = json.NewEncoder(w).Encode(successResponse)
				return
			}

			errorResponse.Status = false
			errorResponse.Message = err.Error()
			_ = json.NewEncoder(w).Encode(errorResponse)
			return
		}

		errorResponse.Status = false
		errorResponse.Message = "Username already Exists!!"
		_ = json.NewEncoder(w).Encode(errorResponse)
		return
	}
}

func UserHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		username := chi.URLParam(r, "username")

		var errorResponse = dto.UserError{Status: false, Message: username}

		_ = json.NewEncoder(w).Encode(errorResponse)
	}
}
